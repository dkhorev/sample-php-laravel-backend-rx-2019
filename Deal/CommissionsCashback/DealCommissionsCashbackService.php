<?php

namespace App\Services\Deal\CommissionsCashback;

use App\Contracts\Deal\DealCommissionsCashbackContract;
use App\Contracts\Services\Deal\DealCommissionsCashbackServiceContract;
use App\Exceptions\Deal\UnexpectedDealStatusException;
use App\Models\Deal\Deal;
use App\Models\Deal\DealCommissionsCashback;
use App\Models\Deal\DealStatusConstants;
use App\Models\Directory\CommissionConstants;

/**
 * Class DealCommissionsCashbackService
 *
 * Responsible for adding deal commission cashback relation
 * We must calculate only for deals that has VERIFIED state. If not we must set to 0 both cashbacks.
 *
 * @package App\Services\Deal\Statuses
 */
class DealCommissionsCashbackService implements DealCommissionsCashbackServiceContract
{
    const ROLE_OFFER  = 'offer';
    const ROLE_CLIENT = 'client';

    /**
     * @var DealCommissionsCashback
     */
    protected $model;

    /**
     * @var DealCommissionsCashbackContract|DealCommissionsCashback
     */
    private $entity;

    /**
     * DealCommissionsCashbackService constructor.
     *
     * @param DealCommissionsCashback $model
     */
    public function __construct(DealCommissionsCashback $model)
    {
        $this->model = $model;
    }

    /**
     * @param Deal $deal
     *
     * @return DealCommissionsCashbackContract
     * @throws UnexpectedDealStatusException
     */
    public function attachTo(Deal $deal): DealCommissionsCashbackContract
    {
        $this->validateDeal($deal)
             ->createComissionsCashback($deal);

        return $this->entity;
    }


    /**
     * @param Deal $deal
     *
     * @return DealCommissionsCashbackService
     * @throws UnexpectedDealStatusException
     */
    private function validateDeal(Deal $deal)
    {
        if ($deal->status->id !== DealStatusConstants::FINISHED
            && $deal->status->id !== DealStatusConstants::CANCELED) {
            throw new UnexpectedDealStatusException('Deal must have [Finished, Canceled] last status');
        }

        return $this;
    }

    /**
     * @param Deal $deal
     *
     * @return DealCommissionsCashbackService
     */
    protected function createComissionsCashback(Deal $deal)
    {
        $this->entity = $this->model->whereDealId($deal->id)->first();

        if (!$this->entity) {
            /** @var DealCommissionsCashback $entity */
            $this->entity = new $this->model;
            $this->fillData($deal);
        }

        return $this;
    }

    /**
     * @param Deal $deal
     */
    private function fillData(Deal $deal)
    {
        $data = [
            'offer_percent'   => $this->getOfferPercent($deal),
            'client_percent'  => $this->getClientPercent($deal),
            'offer_cashback'  => $this->getOfferCashback($deal),
            'client_cashback' => $this->getClientCashback($deal),
            'status'          => DealCommissionsCashback::STATUS_PENDING,
            'created_at'      => now(),
            'updated_at'      => now(),
            'deal_id'         => $deal->id,
        ];

        $this->entity->fill($data)
                     ->save();
    }

    /**
     * @param Deal $deal
     *
     * @return int
     */
    private function getOfferPercent(Deal $deal)
    {
        return $this->getPercentByRole($deal, static::ROLE_OFFER);
    }

    /**
     * @param Deal $deal
     *
     * @return int
     */
    private function getClientPercent(Deal $deal)
    {
        return $this->getPercentByRole($deal, static::ROLE_CLIENT);
    }

    /**
     * @param Deal $deal
     *
     * @return int
     */
    private function getOfferCashback(Deal $deal)
    {
        $percent = $this->getOfferPercent($deal) / CommissionConstants::ONE_PERCENT;

        return (int)ceil($deal->commissions->offerCommission() * $percent);
    }

    /**
     * @param Deal $deal
     *
     * @return int
     */
    private function getClientCashback(Deal $deal)
    {
        $dealTimers = $deal->timers;

        $percent = $this->getClientPercent($deal) / CommissionConstants::ONE_PERCENT;

        if ($deal->isFinished()) {
            if ($dealTimers->isGoodOfferTime()) {
                return (int)ceil($deal->commissions->clientCommission() * $percent);
            } else {
                return (int)ceil($deal->commissions->clientCommission() * $percent);
            }
        }

        if ($deal->isCanceled()) {
            if ($dealTimers->isGoodOfferTime()) {
                return (int)ceil($deal->commissions->clientCommission() * $percent);
            } else {
                return (int)ceil($deal->commissions->offerCommission() * $percent);
            }
        }

        return 0;
    }

    /**
     * @param Deal   $deal
     * @param string $role
     *
     * @return float|int
     */
    private function getPercentByRole(Deal $deal, string $role)
    {
        if ($this->wasNotVerified($deal)) {
            return 0;
        }

        $dealTimers = $deal->timers;

        $configPath = 'app.deal.commissions.cashback_';

        if ($deal->isFinished()) {
            if ($dealTimers->isGoodOfferTime()) {
                return (int)config($configPath . 'finished.fast.' . $role);
            } else {
                return (int)config($configPath . 'finished.slow.' . $role);
            }
        }

        if ($deal->isCanceled()) {
            if ($dealTimers->isGoodOfferTime()) {
                return (int)config($configPath . 'canceled.fast.' . $role);
            } else {
                return (int)config($configPath . 'canceled.slow.' . $role);
            }
        }

        return 0;
    }

    /**
     * @param Deal $deal
     *
     * @return bool
     */
    private function wasNotVerified(Deal $deal)
    {
        return !$deal->statuses()->where('status_id', DealStatusConstants::VERIFIED)->exists();
    }
}
