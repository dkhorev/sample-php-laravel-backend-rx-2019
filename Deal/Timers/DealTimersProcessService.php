<?php

namespace App\Services\Deal\Timers;

use App\Contracts\Deal\DealStatusConstantsContract;
use App\Contracts\Deal\DealTimersContract;
use App\Contracts\Services\Deal\DealTimersProcessServiceContract;
use App\Contracts\Services\Deal\DealTimersServiceContract;
use App\Events\Deal\DealAfterTimeCheck;
use App\Models\Deal\Deal;
use App\Services\Deal\Statuses\OnTimeCheckDealService;
use App\Services\Traits\Dealable;

/**
 * Class DealTimersProcessService
 *
 * Responsible for adding deal timesr for offer and client, no events
 *
 * @package App\Services\Deal\Timers
 */
class DealTimersProcessService implements DealTimersProcessServiceContract
{
    use Dealable;

    /**
     * Event when deal is fast
     *
     * @var string
     */
    protected $eventAfterTimeCheck = DealAfterTimeCheck::class;

    /**
     * @var DealTimersServiceContract
     */
    protected $dealTimersService;

    /**
     * @var DealStatusConstantsContract
     */
    protected $statusConstants;

    /**
     * @var DealTimersContract
     */
    protected $dealTimers;

    /**
     * Create the event listener.
     *
     * @param DealTimersServiceContract   $dealTimersService
     * @param DealStatusConstantsContract $statusConstants
     */
    public function __construct(
        DealTimersServiceContract $dealTimersService,
        DealStatusConstantsContract $statusConstants
    ) {
        $this->dealTimersService = $dealTimersService;
        $this->statusConstants = $statusConstants;
    }

    /**
     * @param Deal $deal
     *
     * @return mixed
     * @throws \Exception
     */
    public function process(Deal $deal)
    {
        $this->setDeal($deal)
             ->attachDealTimers();

        if ($this->hasGoodDealTimers()) {
            event(app($this->eventAfterTimeCheck, ['deal' => $deal]));
        } else {
            /* @var OnTimeCheckDealService $service */
            $service = app(OnTimeCheckDealService::class);
            $service->onTimeCheck($deal);
        }

        return true;
    }

    /**
     * @return $this
     * @throws \App\Exceptions\Deal\UnexpectedDealStatusException
     */
    protected function attachDealTimers()
    {
        $this->dealTimers = $this->dealTimersService->attachTo($this->deal);

        return $this;
    }

    /**
     * @return bool
     */
    protected function hasGoodDealTimers()
    {
        return $this->dealTimers->isGoodTotalTime();
    }
}
