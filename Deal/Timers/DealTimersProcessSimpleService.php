<?php

namespace App\Services\Deal\Timers;

use App\Contracts\Deal\DealStatusConstantsContract;
use App\Contracts\Deal\DealTimersContract;
use App\Contracts\Services\Deal\DealTimersProcessServiceContract;
use App\Contracts\Services\Deal\DealTimersServiceContract;
use App\Models\Deal\Deal;
use App\Services\Traits\Dealable;

/**
 * Class DealTimersProcessSimpleService
 *
 * Responsible for adding deal timers only, for offer and client, no events
 *
 * @package App\Services\Deal\Timers
 */
class DealTimersProcessSimpleService implements DealTimersProcessServiceContract
{
    use Dealable;

    /**
     * @var DealTimersServiceContract
     */
    protected $dealTimersService;

    /**
     * @var DealStatusConstantsContract
     */
    protected $statusConstants;

    /**
     * @var DealTimersContract
     */
    protected $dealTimers;

    /**
     * Create the event listener.
     *
     * @param DealTimersServiceContract   $dealTimersService
     * @param DealStatusConstantsContract $statusConstants
     */
    public function __construct(
        DealTimersServiceContract $dealTimersService,
        DealStatusConstantsContract $statusConstants
    ) {
        $this->dealTimersService = $dealTimersService;
        $this->statusConstants = $statusConstants;
    }

    /**
     * @param Deal $deal
     *
     * @return mixed
     * @throws \Exception
     */
    public function process(Deal $deal)
    {
        $this->setDeal($deal)
             ->attachDealTimers();

        return true;
    }

    /**
     * @return $this
     * @throws \App\Exceptions\Deal\UnexpectedDealStatusException
     */
    protected function attachDealTimers()
    {
        $this->dealTimers = $this->dealTimersService->attachTo($this->deal);

        return $this;
    }
}
