<?php

namespace App\Services\Deal\Commission;

use App\Contracts\Services\Deal\Commission\CommissionStorageContract;
use App\Models\Ad\Ad;
use App\Models\User\User;

/**
 * Class AbstractCommissionService is base class for commission strategies objects
 *
 * @package App\Services\Deal\Commission
 */
abstract class AbstractCommissionService
{
    const ROLE_OFFER  = 'offer';
    const ROLE_CLIENT = 'client';

    /**
     * @var int
     */
    protected $cryptoAmount;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Ad
     */
    protected $ad;

    /**
     * @var int
     */
    protected $commission;

    /**
     * @var int
     */
    protected $percent;

    /**
     * @var string
     */
    protected $role;

    /**
     * Set commission data to local fields
     *
     * @param int  $cryptoAmount
     * @param User $user
     * @param Ad   $ad
     *
     * @return $this
     */
    protected function setCommissionData(int $cryptoAmount, User $user, Ad $ad)
    {
        $this->cryptoAmount = $cryptoAmount;
        $this->user = $user;
        $this->ad = $ad;

        return $this;
    }

    /**
     * Set calculated commission
     *
     * @param int $commission
     *
     * @return $this
     */
    protected function setCommission(int $commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Set user's commission percent
     *
     * @param int $percent
     *
     * @return $this
     */
    protected function setPercent(int $percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Set user's role in deal
     *
     * @param string $role
     *
     * @throws \Exception
     */
    protected function setRole(string $role)
    {
        if (in_array($role, [self::ROLE_CLIENT, self::ROLE_OFFER])) {
            $this->role = $role;

            return;
        }

        throw new \Exception('Unexpected role value "' . $role . '"!');
    }

    /**
     * Generate and retrieve commission storage object
     *
     * @return CommissionStorageContract
     */
    protected function getStorage(): CommissionStorageContract
    {
        return new CommissionStorage(
            $this->commission,
            $this->percent,
            $this->user,
            $this->ad,
            $this->role
        );
    }
}
