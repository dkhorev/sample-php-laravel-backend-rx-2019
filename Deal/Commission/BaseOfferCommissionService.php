<?php

namespace App\Services\Deal\Commission;

use App\Contracts\Services\Deal\Commission\OfferCommissionServiceContract;

/**
 * Class BaseOfferCommissionService
 *
 * @package App\Services\Deal\Commission
 */
class BaseOfferCommissionService extends AbstractBaseCommissionService implements OfferCommissionServiceContract
{
    /**
     * AbstractCommissionService constructor.
     *
     * @param array $config
     *
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->setRole(self::ROLE_OFFER);
    }
}
