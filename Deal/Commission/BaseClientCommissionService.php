<?php

namespace App\Services\Deal\Commission;

use App\Contracts\Services\Deal\Commission\ClientCommissionServiceContract;
use App\Contracts\Services\Deal\Commission\CommissionStorageContract;
use App\Models\Ad\Ad;
use App\Models\Directory\CommissionConstants;
use App\Models\User\User;

/**
 * Class BaseClientCommissionService
 *
 * @package App\Services\Deal\Commission
 */
class BaseClientCommissionService extends AbstractBaseCommissionService implements ClientCommissionServiceContract
{
    /**
     * @var array
     */
    private $minCommissionsFiat;

    /**
     * @var array
     */
    private $minCommissionDiscounts;

    /**
     * AbstractCommissionService constructor.
     *
     * @param array $config
     *
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->setRole(self::ROLE_CLIENT);
        $this->minCommissionsFiat = config('app.deal.commissions.min_commission_in_fiat');
        $this->minCommissionDiscounts = config('app.deal.commissions.user_min_commission_discounts');
    }

    /**
     * {@inheritdoc}
     *
     * @param int  $cryptoAmount
     * @param User $user
     * @param Ad   $ad
     *
     * @return CommissionStorageContract
     */
    public function commission(int $cryptoAmount, User $user, Ad $ad): CommissionStorageContract
    {
        $this->setCommissionData($cryptoAmount, $user, $ad);

        $commission = $this->getDefaultCommission();

        $this->applyMinCommissionDiscountRules($user);

        $commission = $this->applyMinCommissionRules($commission, $ad);

        return $this->setCommission($commission)->getStorage();
    }

    /**
     * Retrieve commission take into consideration personal user percent
     *
     * @return int
     */
    private function getDefaultCommission(): int
    {
        $this->setPercent($this->getPersonalPercent() ?? $this->percent);

        return (int)ceil(
            $this->cryptoAmount * $this->percent / CommissionConstants::ONE_PERCENT / 100
        );
    }

    /**
     * Modifies array on min commissions in fiat for specified user
     *
     * @param User $user
     */
    private function applyMinCommissionDiscountRules(User $user): void
    {
        $discount = $user->getBalance($this->ad->cryptoCurrency)->minCommissionDiscount;
        if ($discount) {
            collect($this->minCommissionsFiat)->each(function ($item, $key) use ($discount) {
                $discountInFiat = ceil($item * $discount->discount() / CommissionConstants::ONE_PERCENT / 100);
                $this->minCommissionsFiat[$key] = $item - $discountInFiat;
            });
        }
    }

    /**
     * @param int $commission
     * @param Ad  $ad
     *
     * @return int
     */
    private function applyMinCommissionRules(int $commission, Ad $ad): int
    {
        $fiatCode = $ad->currency->getCode();

        if (array_key_exists($fiatCode, $this->minCommissionsFiat)) {
            $commissionInFiat = (float)currencyFromCoins($commission, $ad->cryptoCurrency) * $ad->accuracyPrice;
            $minCommissionInFiat = (float)currencyFromCoins($this->minCommissionsFiat[$fiatCode], $ad->currency);

            if ($commissionInFiat < $minCommissionInFiat) {
                $commission = $this->getCommissionInCoins($ad, $minCommissionInFiat);
            }
        }

        return $commission;
    }

    /**
     * Convert commission to coins
     *
     * @param Ad    $ad
     * @param float $minCommissionInFiat
     *
     * @return int
     */
    private function getCommissionInCoins(Ad $ad, float $minCommissionInFiat): int
    {
        return currencyToCoins($minCommissionInFiat / $ad->accuracyPrice, $ad->cryptoCurrency);
    }
}
