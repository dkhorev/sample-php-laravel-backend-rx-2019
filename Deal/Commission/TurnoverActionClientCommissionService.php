<?php

namespace App\Services\Deal\Commission;

use App\Contracts\Repositories\Statistics\TurnoverDiscountActionRepoContract;
use App\Contracts\Services\Deal\Commission\ClientCommissionServiceContract;
use App\Contracts\Services\Deal\Commission\CommissionStorageContract;
use App\Models\Ad\Ad;
use App\Models\User\User;
use App\Services\Deal\Commission\Traits\TurnoverDiscountActionable;

/**
 * Class TurnoverActionClientCommissionService
 * this service is aware of user's 30 day turnover and discounts based on that
 *
 * @package App\Services\Deal\Commission
 */
class TurnoverActionClientCommissionService extends BaseClientCommissionService implements
    ClientCommissionServiceContract
{
    use TurnoverDiscountActionable;

    /**
     * TurnoverActionOfferCommissionService constructor.
     *
     * @param array                              $config
     * @param TurnoverDiscountActionRepoContract $turnoverDiscountActionRepo
     *
     * @throws \Exception
     */
    public function __construct(array $config, TurnoverDiscountActionRepoContract $turnoverDiscountActionRepo)
    {
        parent::__construct($config);
        $this->turnoverDiscountActionRepo = $turnoverDiscountActionRepo;
    }

    /**
     * {@inheritdoc}
     *
     * @param int  $cryptoAmount
     * @param User $user
     * @param Ad   $ad
     *
     * @return CommissionStorageContract
     */
    public function commission(int $cryptoAmount, User $user, Ad $ad): CommissionStorageContract
    {
        if ($this->hasActiveAction($user, $ad->cryptoCurrency)) {
            $this->setPercent(0);
            $this->setCommissionData($cryptoAmount, $user, $ad);

            return $this->setCommission(0)->getStorage();
        }

        return parent::commission($cryptoAmount, $user, $ad);
    }
}
