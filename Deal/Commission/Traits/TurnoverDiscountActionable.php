<?php

namespace App\Services\Deal\Commission\Traits;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Currency\CryptoCurrencyContract;
use App\Contracts\Repositories\Statistics\TurnoverDiscountActionRepoContract;
use App\Models\Statistics\TurnoverDiscountAction;

trait TurnoverDiscountActionable
{
    /**
     * @var TurnoverDiscountActionRepoContract
     */
    protected $turnoverDiscountActionRepo;

    /**
     * @param AuthenticatedContract  $user
     * @param CryptoCurrencyContract $crypto
     *
     * @return bool
     */
    protected function hasActiveAction(AuthenticatedContract $user, CryptoCurrencyContract $crypto)
    {
        $result = $this->turnoverDiscountActionRepo->filterStatus(TurnoverDiscountAction::STATUS_ACTIVE)
                                                   ->filterCrypto($crypto)
                                                   ->filterUser($user)
                                                   ->take()
                                                   ->first();

        return $result ? true : false;
    }
}
