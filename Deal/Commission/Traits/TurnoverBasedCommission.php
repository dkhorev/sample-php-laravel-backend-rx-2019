<?php

namespace App\Services\Deal\Commission\Traits;

use App\Contracts\Repositories\Statistics\UserDailyTurnoverRepoContract;
use App\Models\Ad\Ad;
use App\Models\User\User;

trait TurnoverBasedCommission
{
    /**
     * @var UserDailyTurnoverRepoContract
     */
    protected $userDailyTurnoverRepo;

    /**
     * @var int
     */
    private $freeWithTurnover;

    /**
     * @var int
     */
    private $turnoverPeriod;

    /**
     * @param int  $freeWithTurnover
     * @param User $user
     * @param Ad   $ad
     *
     * @return bool
     */
    private function turnoverMoreThan(int $freeWithTurnover, User $user, Ad $ad)
    {
        $from = now()->subDays($this->turnoverPeriod);

        $sum = $this->userDailyTurnoverRepo->filterUser($user)
                                           ->filterCrypto($ad->cryptoCurrency)
                                           ->filterPeriod($from, now())
                                           ->take()
                                           ->get('turnover')
                                           ->sum('turnover');

        return $sum >= $freeWithTurnover;
    }
}
