<?php

namespace App\Services\Deal\Commission;

use App\Contracts\Services\Deal\Commission\CommissionStorageContract;
use App\Models\Ad\Ad;
use App\Models\User\User;

/**
 * Deal commission storage class
 *
 * @package App\Services\Deal\Commission
 */
class CommissionStorage implements CommissionStorageContract
{
    /**
     * @var int
     */
    private $amount;
    /**
     * @var int
     */
    private $percent;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Ad
     */
    private $ad;
    /**
     * @var string
     */
    private $role;

    /**
     * CommissionStorage constructor.
     *
     * @param int    $amount
     * @param int    $percent
     * @param User   $user
     * @param Ad     $ad
     * @param string $role
     */
    public function __construct(
        int $amount,
        int $percent,
        User $user,
        Ad $ad,
        string $role
    ) {
        $this->amount  = $amount;
        $this->percent = $percent;
        $this->user    = $user;
        $this->ad      = $ad;
        $this->role    = $role;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }

    /**
     * {@inheritdoc}
     *
     * @return int
     */
    public function percent(): int
    {
        return $this->percent;
    }

    /**
     * {@inheritdoc}
     *
     * @return User
     */
    public function user(): User
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     *
     * @return Ad
     */
    public function ad(): Ad
    {
        return $this->ad;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function role(): string
    {
        return $this->role;
    }
}
