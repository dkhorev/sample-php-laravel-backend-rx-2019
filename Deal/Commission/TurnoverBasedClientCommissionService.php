<?php

namespace App\Services\Deal\Commission;

use App\Contracts\Repositories\Statistics\UserDailyTurnoverRepoContract;
use App\Contracts\Services\Deal\Commission\ClientCommissionServiceContract;
use App\Contracts\Services\Deal\Commission\CommissionStorageContract;
use App\Models\Ad\Ad;
use App\Models\User\User;
use App\Services\Deal\Commission\Traits\TurnoverBasedCommission;

/**
 * Class TurnoverBasedClientCommissionService
 * this service is aware of user's 30 day turnover and discounts based on that
 *
 * @package App\Services\Deal\Commission
 */
class TurnoverBasedClientCommissionService extends BaseClientCommissionService implements
    ClientCommissionServiceContract
{
    use TurnoverBasedCommission;

    /**
     * TurnoverBasedClientCommissionService constructor.
     *
     * @param array                         $config
     * @param UserDailyTurnoverRepoContract $userDailyTurnoverRepo
     *
     * @throws \Exception
     */
    public function __construct(array $config, UserDailyTurnoverRepoContract $userDailyTurnoverRepo)
    {
        parent::__construct($config);
        $this->freeWithTurnover = config('app.deal.commissions.client_free_with_turnover');
        $this->turnoverPeriod = config('app.deal.commissions.turnover_period');
        $this->userDailyTurnoverRepo = $userDailyTurnoverRepo;
    }

    /**
     * {@inheritdoc}
     *
     * @param int  $cryptoAmount
     * @param User $user
     * @param Ad   $ad
     *
     * @return CommissionStorageContract
     */
    public function commission(int $cryptoAmount, User $user, Ad $ad): CommissionStorageContract
    {
        if ($this->turnoverMoreThan($this->freeWithTurnover, $user, $ad)) {
            $this->setPercent(0);
            $this->setCommissionData($cryptoAmount, $user, $ad);

            return $this->setCommission(0)->getStorage();
        }

        return parent::commission($cryptoAmount, $user, $ad);
    }
}
