<?php

namespace App\Services\Deal\Commission;

use App\Contracts\Services\Deal\Commission\CommissionStorageContract;
use App\Contracts\Services\Deal\Commission\OfferCommissionServiceContract;
use App\Models\Ad\Ad;
use App\Models\Directory\CommissionConstants;
use App\Models\User\User;

/**
 * Class AbstractBaseCommissionService
 *
 * @package App\Services\Deal\Commission
 */
abstract class AbstractBaseCommissionService extends AbstractCommissionService implements OfferCommissionServiceContract
{
    /**
     * @var array
     */
    protected $config;

    /**
     * AbstractCommissionService constructor.
     *
     * @param array $config
     *
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->checkPercent()->setPercent($this->config['percent']);
    }

    /**
     * Check percent value
     *
     * @return $this
     * @throws \Exception
     */
    private function checkPercent()
    {
        if (isset($this->config['percent']) && is_int($this->config['percent'])) {
            return $this;
        }

        throw new \Exception('Unexpected percent value in configuration array!');
    }

    /**
     * {@inheritdoc}
     *
     * @param int  $cryptoAmount
     * @param User $user
     * @param Ad   $ad
     *
     * @return CommissionStorageContract
     */
    public function commission(int $cryptoAmount, User $user, Ad $ad): CommissionStorageContract
    {
        $this->setCommissionData($cryptoAmount, $user, $ad);

        $this->setPercent($this->getPersonalPercent() ?? $this->percent);

        $commission = (int)ceil(
            $this->cryptoAmount * $this->percent / CommissionConstants::ONE_PERCENT / 100
        );

        return $this->setCommission($commission)->getStorage();
    }

    /**
     * Retrieve personal commission for user by role
     *
     * @return int|null
     */
    protected function getPersonalPercent(): ?int
    {
        $field = $this->role.'_commission';
        return $this->user->getBalance($this->ad->cryptoCurrency)->$field;
    }
}
