<?php

namespace App\Services\Deal;

use App\Contracts\AuthenticatedContract;
use App\Contracts\Services\Deal\TrustedAdMatcherServiceContract;
use App\Models\Ad\Ad;
use App\Models\Directory\Bank;
use App\Models\Directory\CryptoCurrency;
use App\Models\Directory\Currency;
use App\Models\Directory\PaymentSystem;
use App\Repositories\Ad\AdRepo;
use App\Rules\Deal\CheckAmountRule;
use App\Rules\Deal\CheckClientMinCommissionRule;
use App\Rules\Deal\CheckCryptoAmountMinLimitRule;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class TrustedAdMatcherService implements TrustedAdMatcherServiceContract
{
    const SEARCH_NUMBER = 20;

    /**
     * Every Ad must validate by this rules
     *
     * @var array
     */
    private $validationRules = [
        'fiat_amount' => [
            CheckAmountRule::class,
            CheckCryptoAmountMinLimitRule::class,
            CheckClientMinCommissionRule::class,
        ],
    ];

    /**
     * @var AdRepo
     */
    protected $adRepo;

    public function __construct(AdRepo $adRepo)
    {
        $this->adRepo = $adRepo;
    }

    /**
     * @param array                 $data
     * @param AuthenticatedContract $user
     *
     * @return Ad|null
     */
    public function findFor(array $data, AuthenticatedContract $user)
    {
        $this->applyFilters($data, $user);

        /** @var Collection|Ad $ads */
        $ads = $this->adRepo->take()->paginate(static::SEARCH_NUMBER);

        $ads = $ads->filter(function ($ad) use ($data) {
            return $this->isValid($ad, $data);
        });

        // randomize | todo not tested, need to implement own random generator that you can mock
        if (array_key_exists('top', $data) && $ads->count() > 1) {
            Log::info('Trusted deal top:', [
                'ads'     => $ads->pluck(['id'])->toArray(),
                'authors' => $ads->pluck(['author_id'])->toArray(),
                'prices'  => $ads->pluck(['price'])->toArray(),
                'min'     => $ads->pluck(['min'])->toArray(),
                'max'     => $ads->pluck(['max'])->toArray(),
            ]);

            return $ads->take($data['top'])->shuffle()->random();
        }

        return $ads ? $ads->first() : null;
    }

    /**
     * @param array                 $data
     * @param AuthenticatedContract $user
     *
     * @return $this
     */
    private function applyFilters(array $data, AuthenticatedContract $user)
    {
        $this->adRepo->reset();

        $this->adRepo->filterTrustedOffers($user)
                     ->filterAvailableFor($user)
                     ->filterForActive();

        if (array_key_exists('payment_system_id', $data)) {
            $this->adRepo->filterByPaymentSystem(PaymentSystem::findOrFail($data['payment_system_id']));
        }

        if (array_key_exists('bank_id', $data)) {
            $this->adRepo->filterByBank(Bank::findOrFail($data['bank_id']));
        }

        if (array_key_exists('currency_id', $data)) {
            $this->adRepo->filterByCurrency(Currency::findOrFail($data['currency_id']));
        }

        if (array_key_exists('crypto_currency_id', $data)) {
            $this->adRepo->filterByCryptoCurrency(CryptoCurrency::findOrFail($data['crypto_currency_id']));
        }

        if (array_key_exists('is_sale', $data)) {
            $data['is_sale'] ? $this->adRepo->filterForSale() : $this->adRepo->filterForBuy();
            $data['is_sale'] ? $this->adRepo->orderPriceSale() : $this->adRepo->orderPriceBuy();
        }

        return $this;
    }

    /**
     * Check if all validation rules pass
     *
     * @param       $ad
     * @param array $data
     *
     * @return bool
     */
    private function isValid($ad, array $data)
    {
        $result = collect($this->validationRules)->map(function ($rules, $field) use ($ad, $data) {
            return collect($rules)->map(function ($ruleName) use ($ad, $data, $field) {
                /** @var Rule $rule */
                $rule = app($ruleName, ['ad' => $ad, 'data' => $data]);

                return $rule->passes($field, $data[$field]);
            });
        });

        $result = array_filter($result->flatten()->toArray());

        return count($result) === count(collect($this->validationRules)->flatten());
    }
}
