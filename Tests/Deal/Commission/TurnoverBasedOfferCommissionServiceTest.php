<?php

namespace Tests\Unit\Services\Deal\Commission;

use App\Contracts\Services\Deal\Commission\OfferCommissionServiceContract;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\Statistics\UserDailyTurnover;
use App\Services\Deal\Commission\BaseOfferCommissionService;
use App\Services\Deal\Commission\TurnoverBasedOfferCommissionService;
use Illuminate\Support\Facades\Event;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestCase;

class TurnoverBasedOfferCommissionServiceTest extends TestCase
{
    use AdHelpers;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /** @test */
    public function testInstantiate()
    {
        /** @var OfferCommissionServiceContract $service */
        $service = app(TurnoverBasedOfferCommissionService::class, ['config' => ['percent' => 0]]);

        $this->assertInstanceOf(OfferCommissionServiceContract::class, $service);
        $this->assertInstanceOf(TurnoverBasedOfferCommissionService::class, $service);
        $this->assertInstanceOf(BaseOfferCommissionService::class, $service);
    }

    public function typeDataProvider()
    {
        return [
            'sell ad' => [true],
            'buy ad'  => [false],
        ];
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $is_sale
     *
     * @throws \Exception
     */
    public function testStandardOfferCommissionWithoutTurnover(bool $is_sale)
    {
        // setup
        $offer = $this->registerUser();
        $crypto = $this->getCryptoByCode('btc');
        $this->makeAd($offer, ['is_sale' => $is_sale, 'crypto_currency_id' => $crypto]);
        $ad = $this->lastAd($offer);

        // act
        /** @var OfferCommissionServiceContract $service */
        $service = app(TurnoverBasedOfferCommissionService::class, ['config' => ['percent' => 5000]]); // 50%
        $result = $service->commission(CryptoCurrencyConstants::ONE_BTC, $offer, $ad);

        // assert
        $this->assertEquals(CryptoCurrencyConstants::HALF_BTC, $result->amount());
        $this->assertEquals(5000, $result->percent());
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $is_sale
     *
     * @throws \Exception
     */
    public function testZeroOfferCommissionWithMaxTurnover(bool $is_sale)
    {
        // setup
        $offer = $this->registerUser();
        $crypto = $this->getCryptoByCode('btc');
        $this->makeAd($offer, ['is_sale' => $is_sale, 'crypto_currency_id' => $crypto]);
        $ad = $this->lastAd($offer);

        // mock turnover config
        config()->set('app.deal.commissions.offer_free_with_turnover', 10000);

        // mock Offer's turnover
        factory(UserDailyTurnover::class)->create(
            ['user_id'     => $offer,
             'turnover'    => 5000,
             'crypto_code' => $crypto->getCode(),
             'date'        => now()->startOfDay(),
            ]
        );
        factory(UserDailyTurnover::class)->create(
            ['user_id'     => $offer,
             'turnover'    => 5000,
             'crypto_code' => $crypto->getCode(),
             'date'        => now()->subDay()->startOfDay(),
            ]
        );

        // act
        /** @var OfferCommissionServiceContract $service */
        $service = app(TurnoverBasedOfferCommissionService::class, ['config' => ['percent' => 5000]]); // 50%
        $result = $service->commission(CryptoCurrencyConstants::ONE_BTC, $offer, $ad);

        // assert
        $this->assertEquals(0, $result->amount());
        $this->assertEquals(0, $result->percent());
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $is_sale
     *
     * @throws \Exception
     */
    public function testOfferNotZeroIfTurnoverWasLongAgo(bool $is_sale)
    {
        // setup
        $offer = $this->registerUser();
        $crypto = $this->getCryptoByCode('btc');
        $this->makeAd($offer, ['is_sale' => $is_sale, 'crypto_currency_id' => $crypto]);
        $ad = $this->lastAd($offer);

        // mock turnover config
        config()->set('app.deal.commissions.offer_free_with_turnover', 10000);
        config()->set('app.deal.commissions.turnover_period', 30); // 1 month

        // mock Client's turnover
        factory(UserDailyTurnover::class)->create([
            'user_id'     => $offer,
            'turnover'    => 10000 * 10,
            'crypto_code' => $crypto->getCode(),
            'date'        => now()->subMonths(3)->startOfDay(), // 3 months ago
        ]);

        // act
        /** @var OfferCommissionServiceContract $service */
        $service = app(TurnoverBasedOfferCommissionService::class, ['config' => ['percent' => 5000]]); // 50%
        $result = $service->commission(CryptoCurrencyConstants::ONE_BTC, $offer, $ad);

        // assert
        $this->assertEquals(CryptoCurrencyConstants::HALF_BTC, $result->amount());
        $this->assertEquals(5000, $result->percent());
    }
}
