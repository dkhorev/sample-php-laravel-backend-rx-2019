<?php

namespace Tests\Unit\Services\Deal\Commission;

use App\Contracts\Services\Deal\Commission\OfferCommissionServiceContract;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\Statistics\TurnoverDiscountAction;
use App\Services\Deal\Commission\BaseOfferCommissionService;
use App\Services\Deal\Commission\TurnoverActionOfferCommissionService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestCase;

class TurnoverActionOfferCommissionServiceTest extends TestCase
{
    use AdHelpers;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /** @test */
    public function testInstantiate()
    {
        /** @var OfferCommissionServiceContract $service */
        $service = app(TurnoverActionOfferCommissionService::class, ['config' => ['percent' => 0]]);

        $this->assertInstanceOf(OfferCommissionServiceContract::class, $service);
        $this->assertInstanceOf(TurnoverActionOfferCommissionService::class, $service);
        $this->assertInstanceOf(BaseOfferCommissionService::class, $service);
    }

    public function typeDataProvider()
    {
        return [
            'sell ad' => [true],
            'buy ad'  => [false],
        ];
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $is_sale
     *
     * @throws \Exception
     */
    public function testStandardOfferCommissionWithoutAction(bool $is_sale)
    {
        // setup
        $offer = $this->registerUser();
        $crypto = $this->getCryptoByCode('btc');
        $this->makeAd($offer, ['is_sale' => $is_sale, 'crypto_currency_id' => $crypto]);
        $ad = $this->lastAd($offer);

        // act
        /** @var OfferCommissionServiceContract $service */
        $service = app(TurnoverActionOfferCommissionService::class, ['config' => ['percent' => 5000]]); // 50%
        $result = $service->commission(CryptoCurrencyConstants::ONE_BTC, $offer, $ad);

        // assert
        $this->assertEquals(CryptoCurrencyConstants::HALF_BTC, $result->amount());
        $this->assertEquals(5000, $result->percent());
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $is_sale
     *
     * @throws \Exception
     */
    public function testZeroOfferCommissionWithActiveAction(bool $is_sale)
    {
        // setup
        $offer = $this->registerUser();
        $crypto = $this->getCryptoByCode('btc');
        $this->makeAd($offer, ['is_sale' => $is_sale, 'crypto_currency_id' => $crypto]);
        $ad = $this->lastAd($offer);

        // mock action
        factory(TurnoverDiscountAction::class)->create([
            'user_id'     => $offer->id,
            'crypto_code' => $crypto->getCode(),
            'turnover'    => 9999,
            'status'      => TurnoverDiscountAction::STATUS_ACTIVE,
        ]);

        // act
        /** @var OfferCommissionServiceContract $service */
        $service = app(TurnoverActionOfferCommissionService::class, ['config' => ['percent' => 5000]]); // 50%
        $result = $service->commission(CryptoCurrencyConstants::ONE_BTC, $offer, $ad);

        // assert
        $this->assertEquals(0, $result->amount());
        $this->assertEquals(0, $result->percent());
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $is_sale
     *
     * @throws \Exception
     */
    public function testOfferNotZeroIfOtherRecordTypes(bool $is_sale)
    {
        // setup
        $offer = $this->registerUser();
        $crypto = $this->getCryptoByCode('btc');
        $this->makeAd($offer, ['is_sale' => $is_sale, 'crypto_currency_id' => $crypto]);
        $ad = $this->lastAd($offer);

        // mock action
        factory(TurnoverDiscountAction::class)->create([
            'user_id'     => $offer->id,
            'crypto_code' => $crypto->getCode(),
            'turnover'    => 9999,
            'status'      => Arr::random([
                TurnoverDiscountAction::STATUS_FINISHED,
                TurnoverDiscountAction::STATUS_PROGRESS,
            ]),
        ]);

        // act
        /** @var OfferCommissionServiceContract $service */
        $service = app(TurnoverActionOfferCommissionService::class, ['config' => ['percent' => 5000]]); // 50%
        $result = $service->commission(CryptoCurrencyConstants::ONE_BTC, $offer, $ad);

        // assert
        $this->assertEquals(CryptoCurrencyConstants::HALF_BTC, $result->amount());
        $this->assertEquals(5000, $result->percent());
    }
}
