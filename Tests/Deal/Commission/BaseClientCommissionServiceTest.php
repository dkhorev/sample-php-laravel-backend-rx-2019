<?php

namespace Tests\Unit\Services\Deal\Commission;

use App\Contracts\Services\Deal\Commission\ClientCommissionServiceContract;
use App\Models\Ad\Ad;
use App\Models\Balance\MinCommissionDiscount;
use App\Models\Deal\Deal;
use App\Models\Directory\CommissionConstants;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\User\User;
use App\Services\Deal\Commission\BaseClientCommissionService;
use App\Services\Deal\StoreDealService;
use Illuminate\Support\Facades\Event;
use Tests\TestApi\Traits\AdHelpers;
use Tests\TestCase;

class BaseClientCommissionServiceTest extends TestCase
{
    use AdHelpers;

    /**
     * @var User;
     */
    private $offer;

    /**
     * @var User
     */
    private $client;

    /**
     * @var Ad
     */
    private $ad;

    /**
     * @var Deal
     */
    private $deal;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /** @test */
    public function testInstantiate()
    {
        /** @var ClientCommissionServiceContract $service */
        $service = app(BaseClientCommissionService::class, ['config' => ['percent' => 0]]);

        $this->assertInstanceOf(ClientCommissionServiceContract::class, $service);
        $this->assertInstanceOf(BaseClientCommissionService::class, $service);
    }

    public function typeDataProvider()
    {
        return [
            'sell ad' => [true],
            'buy ad'  => [false],
        ];
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $createAdForSale
     *
     * @throws \Exception
     */
    public function testMinClientCommissionTaken(bool $createAdForSale)
    {
        // setup configs
        config()->set('app.deal.commissions.min_commission_in_fiat.RUB', 3500);

        $this->initTestData($createAdForSale);

        $this->storeDeal();

        // assert
        $expected = currencyToCoins(
            currencyFromCoins(3500, $this->ad->currency) / $this->ad->accuracyPrice,
            $this->ad->cryptoCurrency
        );
        $this->assertEquals($expected, $this->deal->commissions->clientCommission());
    }

    /**
     * Prepare test data
     *
     * @param bool $createAdForSale
     *
     * @return $this
     */
    protected function initTestData(bool $createAdForSale)
    {
        $this->offer  = $this->registerUser();
        $this->client = $this->registerUser();

        $this->makeAd($this->offer, ['is_sale' => $createAdForSale, 'currency_id' => 1, 'price' => 70000000]);
        $this->ad = $this->lastAd($this->offer);

        $crypto = $this->ad->cryptoCurrency;

        $this->setBalance($this->offer, $crypto, CryptoCurrencyConstants::ONE_BTC);
        $this->setBalance($this->client, $crypto, CryptoCurrencyConstants::ONE_BTC);

        return $this;
    }

    /**
     * Store deal
     *
     * @param int $fiat_amount
     *
     * @return $this
     * @throws \Exception
     */
    protected function storeDeal(int $fiat_amount = 10)
    {
        $this->actingAs($this->client);
        /* @var StoreDealService $service */
        $service = app(StoreDealService::class);
        $this->deal = $service->store([
            'ad_id'       => $this->ad->id,
            'price'       => $this->ad->accuracyPrice,
            'fiat_amount' => $fiat_amount,
        ]);
        return $this;
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $createAdForSale
     *
     * @throws \Exception
     */
    public function testMinClientCommissionIsDiscounted(bool $createAdForSale)
    {
        // setup configs
        $amount = 3500;
        config()->set('app.deal.commissions.min_commission_in_fiat.RUB', $amount);

        $this->initTestData($createAdForSale);

        // mock discount 50%
        $this->client->getBalance($this->ad->cryptoCurrency)->minCommissionDiscount()->save(
            app(MinCommissionDiscount::class)->fill([
                'discount' => 50  *  CommissionConstants::ONE_PERCENT,
            ])
        );

        $this->storeDeal();

        // assert discounted 50%
        $amount = $amount * 50 / 100;
        $expected = currencyToCoins(
            currencyFromCoins($amount, $this->ad->currency) / $this->ad->accuracyPrice,
            $this->ad->cryptoCurrency
        );
        $this->assertEquals($expected, $this->deal->commissions->clientCommission());
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $createAdForSale
     *
     * @throws \Exception
     */
    public function testMinClientCommissionDiscountedTo100Percent(bool $createAdForSale)
    {
        // setup configs
        config()->set('app.deal.commissions.min_commission_in_fiat.RUB', 3500);
        config()->set('app.deal.commissions.client', 50); // 0.5%

        $this->initTestData($createAdForSale);

        // mock discount 100%
        $this->client->getBalance($this->ad->cryptoCurrency)->minCommissionDiscount()->save(
            app(MinCommissionDiscount::class)->fill([
                'discount' => 100 * CommissionConstants::ONE_PERCENT,
            ])
        );

        $this->storeDeal();

        // assert discounted 100%
        $expected = (int)ceil($this->deal->crypto_amount * 50 / 10000);
        $this->assertEquals($expected, $this->deal->commissions->clientCommission());
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $createAdForSale
     *
     * @throws \Exception
     */
    public function testPersonalOfferCommission(bool $createAdForSale)
    {
        // setup configs
        $amount = 3500;
        $personalPercent = 15;
        config()->set('app.deal.commissions.min_commission_in_fiat.RUB', $amount);

        $this->initTestData($createAdForSale);

        $this->offer->getBalance($this->ad->cryptoCurrency)
                    ->fill(['offer_commission' => $personalPercent])
                    ->save();

        $this->storeDeal(100000);

        $expected = (int)ceil(
            ($this->deal->crypto_amount ) * ($personalPercent / CommissionConstants::ONE_PERCENT /100)
        );

        $this->assertEquals($expected, $this->deal->commissions->offerCommission());
    }

    /**
     * @test
     * @dataProvider typeDataProvider
     *
     * @param bool $createAdForSale
     *
     * @throws \Exception
     */
    public function testPersonalClientCommission(bool $createAdForSale)
    {
        // setup configs
        $amount = 3500;
        $personalPercent = 15;
        config()->set('app.deal.commissions.min_commission_in_fiat.RUB', $amount);

        $this->initTestData($createAdForSale);

        $this->client->getBalance($this->ad->cryptoCurrency)
                     ->fill(['client_commission' => $personalPercent])
                     ->save();

        $this->storeDeal(100000);

        $expected = (int)ceil(
            ($this->deal->crypto_amount ) * ($personalPercent / CommissionConstants::ONE_PERCENT /100)
        );

        $this->assertEquals($expected, $this->deal->commissions->clientCommission());
    }
}
