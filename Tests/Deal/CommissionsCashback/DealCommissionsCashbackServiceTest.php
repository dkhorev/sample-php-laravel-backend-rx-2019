<?php

namespace Tests\Unit\Services\Deal\CommissionsCashback;

use App\Contracts\Services\Deal\DealCommissionsCashbackServiceContract;
use App\Exceptions\Deal\UnexpectedDealStatusException;
use App\Models\Deal\Deal;
use App\Models\Deal\DealCommissions;
use App\Models\Deal\DealCommissionsCashback;
use App\Models\Deal\DealStatusConstants;
use App\Models\Deal\DealTimers;
use App\Models\Directory\CommissionConstants;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Services\Deal\CommissionsCashback\DealCommissionsCashbackService;
use Tests\TestCase;

class DealCommissionsCashbackServiceTest extends TestCase
{
    protected function prepareDeal(Deal $deal)
    {
        // setup - deal commissions | needed
        DealCommissions::insert([
            'deal_id'                   => $deal->id,
            'offer_commission'          => 200000,
            'client_commission'         => 100000,
            'offer_commission_percent'  => 80,
            'client_commission_percent' => 40,
        ]);
    }

    /** @test */
    public function testInstantiate()
    {
        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(\App\Services\Deal\CommissionsCashback\DealCommissionsCashbackService::class);

        $this->assertInstanceOf(DealCommissionsCashbackService::class, $service);
        $this->assertInstanceOf(DealCommissionsCashbackServiceContract::class, $service);
    }

    /**
     * @return array
     */
    public function badStatuses()
    {
        return [
            [DealStatusConstants::VERIFIED],
            [DealStatusConstants::VERIFICATION],
            [DealStatusConstants::AUTOCANCELED],
            [DealStatusConstants::CANCELLATION],
            [DealStatusConstants::PAID],
            [DealStatusConstants::FINISHING],
            [DealStatusConstants::IN_DISPUTE],
            [
                function () {
                    return app(DealStatusConstants::class)->statusOnTimeCheck();
                },
            ],
        ];
    }

    /**
     * @test
     * @dataProvider badStatuses
     *
     * @param $status
     *
     * @throws UnexpectedDealStatusException
     */
    public function testExceptionOnBadDealStatus($status)
    {
        // fix dataProvider running before setUp
        if (is_callable($status)) {
            $status = $status();
        }

        /** @var Deal $deal */
        $deal = factory(Deal::class)->create();
        $deal->statuses()->attach($status, ['created_at' => now()]);

        // assert
        $this->expectException(UnexpectedDealStatusException::class);

        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(DealCommissionsCashbackService::class);
        $service->attachTo($deal);
    }

    /**
     * @test
     * @throws UnexpectedDealStatusException
     */
    public function testBlockchainCanceledDealNoCashbacks()
    {
        $dealSum = CryptoCurrencyConstants::ONE_BTC;
        /** @var Deal $deal */
        $deal = factory(Deal::class)->create(['crypto_amount' => $dealSum]);
        // !!! important !!! CANCELED not having VERIFIED status
        $deal->statuses()->attach(DealStatusConstants::CANCELED, ['created_at' => now()]);
        // setup - timers | deal commissions
        $this->prepareDeal($deal);

        // setup - dealtimers needed
        $dealTimers = new DealTimers;
        $dealTimers->dealRelation()->associate($deal);
        $dealTimers->setClientTime(0)
                   ->setOfferTime(0)
                   ->save();

        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(DealCommissionsCashbackService::class);
        $entity = $service->attachTo($deal);

        // assert
        $this->assertEquals($deal->id, $entity->deal()->id);
        $this->assertEquals(0, $entity->offerPercent());
        $this->assertEquals(0, $entity->offerCashback());
        $this->assertEquals(0, $entity->clientCashback());
        $this->assertEquals(0, $entity->clientPercent());
        $this->assertEquals(DealCommissionsCashback::STATUS_PENDING, $entity->status());
        $this->assertNull($entity->transactionId());
        $this->assertNotNull($entity->createdAt());
        $this->assertNotNull($entity->updatedAt());
    }

    /**
     * @test
     * @throws UnexpectedDealStatusException
     */
    public function testFastFinishedDeal()
    {
        $dealSum = CryptoCurrencyConstants::ONE_BTC;
        /** @var Deal $deal */
        $deal = factory(Deal::class)->create(['crypto_amount' => $dealSum]);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::FINISHED, ['created_at' => now()]);
        // setup - timers | deal commissions
        $this->prepareDeal($deal);

        // setup - dealtimers | needed
        $dealTimers = new DealTimers;
        $dealTimers->dealRelation()->associate($deal);
        $dealTimers->setClientTime(0)
                   ->setOfferTime(0)
                   ->save();

        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(DealCommissionsCashbackService::class);
        $entity = $service->attachTo($deal);

        // assert | deal  FINISHED | deal was fast | return cashback to offer
        $percent = config('app.deal.commissions.cashback_finished.fast.offer');
        $cbOffer = $deal->commissions->offerCommission() * $percent / CommissionConstants::ONE_PERCENT;
        $this->assertEquals($deal->id, $entity->deal()->id);
        $this->assertEquals($percent, $entity->offerPercent());
        $this->assertEquals($cbOffer, $entity->offerCashback());
        $this->assertEquals(0, $entity->clientCashback());
        $this->assertEquals(0, $entity->clientPercent());
        $this->assertEquals(DealCommissionsCashback::STATUS_PENDING, $entity->status());
        $this->assertNull($entity->transactionId());
        $this->assertNotNull($entity->createdAt());
        $this->assertNotNull($entity->updatedAt());
    }

    /**
     * @test
     * @throws UnexpectedDealStatusException
     */
    public function testFinishedDealSlowOffer()
    {
        $dealSum = CryptoCurrencyConstants::ONE_BTC;
        /** @var Deal $deal */
        $deal = factory(Deal::class)->create(['crypto_amount' => $dealSum]);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::FINISHED, ['created_at' => now()]);
        // setup - deal commissions
        $this->prepareDeal($deal);

        // setup - dealtimers needed
        $dealTimers = new DealTimers;
        $dealTimers->dealRelation()->associate($deal);
        $dealTimers->setClientTime(0)
                   ->setOfferTime(config('app.deal.long_wait') + 1)// slow offer
                   ->save();

        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(DealCommissionsCashbackService::class);
        $entity = $service->attachTo($deal);

        // assert | deal FINISHED | deal slow OFFER | return cashback to client
        $percentOffer = config('app.deal.commissions.cashback_finished.slow.offer'); // 0%
        $percentClient = config('app.deal.commissions.cashback_finished.slow.client'); // 100% of clients comms
        $cbOffer = $deal->commissions->offerCommission() * $percentOffer / CommissionConstants::ONE_PERCENT;
        $cbClient = $deal->commissions->clientCommission() * $percentClient / CommissionConstants::ONE_PERCENT; // 100% of clients comms
        $this->assertEquals($deal->id, $entity->deal()->id);
        $this->assertEquals($percentOffer, $entity->offerPercent());
        $this->assertEquals($cbOffer, $entity->offerCashback());
        $this->assertEquals($cbClient, $entity->clientCashback());
        $this->assertEquals($percentClient, $entity->clientPercent());
        $this->assertEquals(DealCommissionsCashback::STATUS_PENDING, $entity->status());
        $this->assertNull($entity->transactionId());
        $this->assertNotNull($entity->createdAt());
        $this->assertNotNull($entity->updatedAt());
    }

    /**
     * @test
     * @throws UnexpectedDealStatusException
     */
    public function testFinishedDealSlowClient()
    {
        $dealSum = CryptoCurrencyConstants::ONE_BTC;
        /** @var Deal $deal */
        $deal = factory(Deal::class)->create(['crypto_amount' => $dealSum]);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::FINISHED, ['created_at' => now()]);
        // setup - timers | deal commissions
        $this->prepareDeal($deal);

        // setup - dealtimers needed
        $dealTimers = new DealTimers;
        $dealTimers->dealRelation()->associate($deal);
        $dealTimers->setClientTime(config('app.deal.long_wait') + 1)// slow client
                   ->setOfferTime(0)
                   ->save();

        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(DealCommissionsCashbackService::class);
        $entity = $service->attachTo($deal);

        // assert | deal FINISHED | deal slow CLIENT | return cashback to offer
        $percentOffer = config('app.deal.commissions.cashback_finished.fast.offer'); // 50%
        $percentClient = config('app.deal.commissions.cashback_finished.fast.client'); // 0%
        $cbOffer = $deal->commissions->offerCommission() * $percentOffer / CommissionConstants::ONE_PERCENT;
        $cbClient = $deal->commissions->clientCommission() * $percentClient / CommissionConstants::ONE_PERCENT;
        $this->assertEquals($deal->id, $entity->deal()->id);
        $this->assertEquals($percentOffer, $entity->offerPercent());
        $this->assertEquals($cbOffer, $entity->offerCashback());
        $this->assertEquals($cbClient, $entity->clientCashback());
        $this->assertEquals($percentClient, $entity->clientPercent());
        $this->assertEquals(DealCommissionsCashback::STATUS_PENDING, $entity->status());
        $this->assertNull($entity->transactionId());
        $this->assertNotNull($entity->createdAt());
        $this->assertNotNull($entity->updatedAt());
    }

    /**
     * @test
     * @throws UnexpectedDealStatusException
     */
    public function testFastCanceledDeal()
    {
        $dealSum = CryptoCurrencyConstants::ONE_BTC;
        /** @var Deal $deal */
        $deal = factory(Deal::class)->create(['crypto_amount' => $dealSum]);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::CANCELED, ['created_at' => now()]);
        // setup - timers | deal commissions
        $this->prepareDeal($deal);

        // setup - dealtimers needed
        $dealTimers = new DealTimers;
        $dealTimers->dealRelation()->associate($deal);
        $dealTimers->setClientTime(0)
                   ->setOfferTime(0)
                   ->save();

        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(DealCommissionsCashbackService::class);
        $entity = $service->attachTo($deal);

        // assert | deal CANCELED | deal was fast | return cashback to offer
        $percent = config('app.deal.commissions.cashback_canceled.fast.offer');
        $cashback = $deal->commissions->offerCommission() * $percent / CommissionConstants::ONE_PERCENT;
        $this->assertEquals($deal->id, $entity->deal()->id);
        $this->assertEquals($percent, $entity->offerPercent());
        $this->assertEquals($cashback, $entity->offerCashback());
        $this->assertEquals(0, $entity->clientCashback());
        $this->assertEquals(0, $entity->clientPercent());
        $this->assertEquals(DealCommissionsCashback::STATUS_PENDING, $entity->status());
        $this->assertNull($entity->transactionId());
        $this->assertNotNull($entity->createdAt());
        $this->assertNotNull($entity->updatedAt());
    }

    /**
     * @test
     * @throws UnexpectedDealStatusException
     */
    public function testCanceledDealSlowOffer()
    {
        $dealSum = CryptoCurrencyConstants::ONE_BTC;
        /** @var Deal $deal */
        $deal = factory(Deal::class)->create(['crypto_amount' => $dealSum]);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::CANCELED, ['created_at' => now()]);
        // setup - timers | deal commissions
        $this->prepareDeal($deal);

        // setup - dealtimers needed
        $dealTimers = new DealTimers;
        $dealTimers->dealRelation()->associate($deal);
        $dealTimers->setClientTime(0)
                   ->setOfferTime(config('app.deal.long_wait') + 1)// slow offer
                   ->save();

        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(DealCommissionsCashbackService::class);
        $entity = $service->attachTo($deal);

        // assert | deal CANCLED | deal slow OFFER | return cashback to client and offer
        $percentOffer = config('app.deal.commissions.cashback_canceled.slow.offer'); // 50%
        $percentClient = config('app.deal.commissions.cashback_canceled.slow.client'); // 50% from offer's comm
        $cbOffer = $deal->commissions->offerCommission() * $percentOffer / CommissionConstants::ONE_PERCENT;
        $cbClient = $deal->commissions->offerCommission() * $percentClient / CommissionConstants::ONE_PERCENT;
        $this->assertEquals($deal->id, $entity->deal()->id);
        $this->assertEquals($percentOffer, $entity->offerPercent());
        $this->assertEquals($cbOffer, $entity->offerCashback());
        $this->assertEquals($cbClient, $entity->clientCashback());
        $this->assertEquals($percentClient, $entity->clientPercent());
        $this->assertEquals(DealCommissionsCashback::STATUS_PENDING, $entity->status());
        $this->assertNull($entity->transactionId());
        $this->assertNotNull($entity->createdAt());
        $this->assertNotNull($entity->updatedAt());
    }

    /**
     * @test
     * @throws UnexpectedDealStatusException
     */
    public function testCanceledDealSlowClient()
    {
        $dealSum = CryptoCurrencyConstants::ONE_BTC;
        /** @var Deal $deal */
        $deal = factory(Deal::class)->create(['crypto_amount' => $dealSum]);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::CANCELED, ['created_at' => now()]);
        // setup - timers | deal commissions
        $this->prepareDeal($deal);

        // setup - dealtimers needed
        $dealTimers = new DealTimers;
        $dealTimers->dealRelation()->associate($deal);
        $dealTimers->setClientTime(config('app.deal.long_wait') + 1)// slow client
                   ->setOfferTime(0)
                   ->save();

        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(DealCommissionsCashbackService::class);
        $entity = $service->attachTo($deal);

        // assert | deal CANCELED | deal slow CLIENT | return cashback to offer
        $percentOffer = config('app.deal.commissions.cashback_canceled.fast.offer'); // 50%
        $percentClient = config('app.deal.commissions.cashback_canceled.fast.client'); // 0%
        $cashbackOffer = $deal->commissions->offerCommission() * $percentOffer / CommissionConstants::ONE_PERCENT;
        $cashbackClient = $deal->commissions->clientCommission() * $percentClient / CommissionConstants::ONE_PERCENT;
        $this->assertEquals($deal->id, $entity->deal()->id);
        $this->assertEquals($percentOffer, $entity->offerPercent());
        $this->assertEquals($cashbackOffer, $entity->offerCashback());
        $this->assertEquals($cashbackClient, $entity->clientCashback());
        $this->assertEquals($percentClient, $entity->clientPercent());
        $this->assertEquals(DealCommissionsCashback::STATUS_PENDING, $entity->status());
        $this->assertNull($entity->transactionId());
        $this->assertNotNull($entity->createdAt());
        $this->assertNotNull($entity->updatedAt());
    }

    /**
     * @test
     * @throws UnexpectedDealStatusException
     */
    public function testNoDoubleAttaches()
    {
        $dealSum = CryptoCurrencyConstants::ONE_BTC;
        /** @var Deal $deal */
        $deal = factory(Deal::class)->create(['crypto_amount' => $dealSum]);
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::CANCELED, ['created_at' => now()]);
        // setup - timers | deal commissions
        $this->prepareDeal($deal);

        // setup - dealtimers needed
        $dealTimers = new DealTimers;
        $dealTimers->dealRelation()->associate($deal);
        $dealTimers->setClientTime(config('app.deal.long_wait') + 1)// slow client
                   ->setOfferTime(0)
                   ->save();

        /** @var DealCommissionsCashbackServiceContract $service */
        $service = app(DealCommissionsCashbackService::class);
        $service->attachTo($deal);
        $service->attachTo($deal);

        // assert
        $this->assertEquals(1, DealCommissionsCashback::count());
    }
}
