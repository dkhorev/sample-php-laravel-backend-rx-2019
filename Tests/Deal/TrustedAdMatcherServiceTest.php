<?php

namespace Tests\Unit\Services\Deal;

use App\Contracts\Services\Deal\TrustedAdMatcherServiceContract;
use App\Models\Directory\Bank;
use App\Models\Directory\CryptoCurrencyConstants;
use App\Models\Directory\PaymentSystem;
use App\Services\Deal\TrustedAdMatcherService;
use Tests\TestCase;

class TrustedAdMatcherServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var TrustedAdMatcherServiceContract $service */
        $service = app(TrustedAdMatcherService::class);

        $this->assertInstanceOf(TrustedAdMatcherService::class, $service);
        $this->assertInstanceOf(TrustedAdMatcherServiceContract::class, $service);
    }

    public function adTypeProvider()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @test
     */
    public function testFiltersBanks()
    {
        // setup
        config()->set('app.deal.commissions.min_commission_in_fiat.RUB', 0);
        $client = $this->registerUser();
        $offer1 = $this->registerUser();
        $client->trustedOffers()->attach($offer1);

        $data = [
            'payment_system_id' => PaymentSystem::BANK_SYSTEM_ID,
            'price'             => 500000,
            'min'               => 1000,
            'max'               => 10000,
        ];

        $this->makeAd($offer1, array_merge($data, ['price' => 400000]))
             ->banks()
             ->attach(factory(Bank::class)->create());

        $adExpected = $this->makeAd($offer1, $data);
        $bankExpected = factory(Bank::class)->create();
        $adExpected->banks()->attach($bankExpected);


        // act
        $this->actingAs($client);
        /** @var TrustedAdMatcherServiceContract $service */
        $service = app(TrustedAdMatcherService::class);
        $result = $service->findFor([
            'fiat_amount'       => $adExpected->price / 10000,
            'payment_system_id' => PaymentSystem::BANK_SYSTEM_ID,
            'bank_id'           => $bankExpected->id,
        ], $client);

        // assert
        $this->assertEquals($adExpected->id, $result->id);
    }

    /**
     * @test
     * @dataProvider adTypeProvider
     *
     * @param $isSale
     */
    public function testSelectsCorrectAdsFromDiffTraders($isSale)
    {
        // setup
        config()->set('app.deal.commissions.min_commission_in_fiat.RUB', 0);
        $client = $this->registerUser();
        $offer1 = $this->registerUser();
        $offer2 = $this->registerUser();
        $offer3 = $this->registerUser();
        $this->addBalance($client, $this->getCryptoByCode('btc'), CryptoCurrencyConstants::ONE_BTC);
        $this->addBalance($offer1, $this->getCryptoByCode('btc'), CryptoCurrencyConstants::ONE_BTC);
        $client->trustedOffers()->attach($offer1);

        $data = [
            'payment_system_id'  => 1,
            'currency_id'        => 1,
            'crypto_currency_id' => 1,
            'price'              => 500000,
            'min'                => 1000,
            'max'                => 10000,
            'is_sale'            => $isSale,
        ];
        $adExpected = $this->makeAd($offer1, $data);
        $this->makeAd($offer1, array_merge($data, [
            'payment_system_id' => 2, // any payment_system_id not ID = 1
        ]));

        // other random ads
        $this->makeAd($offer1, array_merge($data, ['is_sale' => !$isSale]));
        $this->makeAd($offer2, array_merge($data, []));
        $this->makeAd($offer2, array_merge($data, ['is_sale' => !$isSale]));
        $this->makeAd($offer2, array_merge($data, ['is_sale' => !$isSale]));
        $this->makeAd($offer3, array_merge($data, []));
        $this->makeAd($offer3, array_merge($data, ['is_sale' => !$isSale]));

        // act
        $this->actingAs($client);
        /** @var TrustedAdMatcherServiceContract $service */
        $service = app(TrustedAdMatcherService::class);
        $result = $service->findFor([
            'fiat_amount'        => $adExpected->price / 10000,
            'payment_system_id'  => 1,
            'currency_id'        => 1,
            'crypto_currency_id' => 1,
            'is_sale'            => $isSale,
        ], $client);

        // assert
        $this->assertEquals($adExpected->id, $result->id);
    }

    /**
     * @test
     * @dataProvider adTypeProvider
     *
     * @param $isSale
     */
    public function testMatcherServiceValidatesMinAmountForDeal($isSale)
    {
        // setup
        $client = $this->registerUser();
        $offer1 = $this->registerUser();
        $client->trustedOffers()->attach($offer1);

        $data = [
            'payment_system_id'  => 1,
            'currency_id'        => 1,
            'crypto_currency_id' => 1,
            'price'              => 5000000000,
            'min'                => 1000,
            'max'                => 10000,
            'is_sale'            => $isSale,
        ];
        $this->makeAd($offer1, $data);

        // act
        $this->actingAs($client);
        /** @var TrustedAdMatcherServiceContract $service */
        $service = app(TrustedAdMatcherService::class);
        $result = $service->findFor([
            'fiat_amount'        => 1, // very small fiat amount compared to Ad->price
            'payment_system_id'  => 1,
            'currency_id'        => 1,
            'crypto_currency_id' => 1,
            'is_sale'            => $isSale,
        ], $client);

        // assert
        $this->assertNull($result);
    }

    /**
     * @test
     * @dataProvider adTypeProvider
     *
     * @param $isSale
     */
    public function testMatcherServiceValidatesMinClientCommissionForDeal($isSale)
    {
        // setup
        config()->set('app.deal.commissions.min_commission_in_fiat.RUB', 50000000); // very high min commission
        $client = $this->registerUser();
        $offer1 = $this->registerUser();
        $client->trustedOffers()->attach($offer1);

        $data = [
            'payment_system_id'  => 1,
            'currency_id'        => 1, // RUB
            'crypto_currency_id' => 1,
            'price'              => 5000,
            'min'                => 1000,
            'max'                => 10000,
            'is_sale'            => $isSale,
        ];
        $this->makeAd($offer1, $data);

        // act
        $this->actingAs($client);
        /** @var TrustedAdMatcherServiceContract $service */
        $service = app(TrustedAdMatcherService::class);
        $result = $service->findFor([
            'fiat_amount'        => 1000,
            'payment_system_id'  => 1,
            'currency_id'        => 1, // RUB
            'crypto_currency_id' => 1,
            'is_sale'            => $isSale,
        ], $client);

        // assert
        $this->assertNull($result);
    }

    /**
     * @test
     * @dataProvider adTypeProvider
     *
     * @param $isSale
     */
    public function testAdMinAmountLimitIsChecked($isSale)
    {
        // setup
        $client = $this->registerUser();
        $offer1 = $this->registerUser();
        $client->trustedOffers()->attach($offer1);

        $data = [
            'payment_system_id'  => 1,
            'currency_id'        => 1, // RUB
            'crypto_currency_id' => 1,
            'price'              => 50000000,
            'min'                => 100000,
            'max'                => 10000000,
            'is_sale'            => $isSale,
        ];
        $this->makeAd($offer1, $data);

        // act
        $this->actingAs($client);
        /** @var TrustedAdMatcherServiceContract $service */
        $service = app(TrustedAdMatcherService::class);
        $result = $service->findFor([
            'fiat_amount'        => 10, // must be low compared with $ad->min
            'payment_system_id'  => 1,
            'currency_id'        => 1, // RUB
            'crypto_currency_id' => 1,
            'is_sale'            => $isSale,
        ], $client);

        // assert
        $this->assertNull($result);
    }

    /**
     * @test
     * @dataProvider adTypeProvider
     *
     * @param $isSale
     */
    public function testAdMaxAmountLimitIsChecked($isSale)
    {
        // setup
        $client = $this->registerUser();
        $offer1 = $this->registerUser();
        $client->trustedOffers()->attach($offer1);

        $data = [
            'payment_system_id'  => 1,
            'currency_id'        => 1, // RUB
            'crypto_currency_id' => 1,
            'price'              => 50000000,
            'min'                => 100000,
            'max'                => 10000000,
            'is_sale'            => $isSale,
        ];
        $this->makeAd($offer1, $data);

        // act
        $this->actingAs($client);
        /** @var TrustedAdMatcherServiceContract $service */
        $service = app(TrustedAdMatcherService::class);
        $result = $service->findFor([
            'fiat_amount'        => 1000000000000, // must be high compared with $ad->max
            'payment_system_id'  => 1,
            'currency_id'        => 1, // RUB
            'crypto_currency_id' => 1,
            'is_sale'            => $isSale,
        ], $client);

        // assert
        $this->assertNull($result);
    }
}
