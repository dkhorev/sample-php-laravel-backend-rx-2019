<?php

namespace Tests\Unit\Services\Deal;

use App\Contracts\Services\Deal\DealTimersProcessServiceContract;
use App\Models\Deal\Deal;
use App\Models\Deal\DealStatusConstants;
use App\Services\Deal\Timers\DealTimersProcessSimpleService;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

/**
 * Class DealTimersProcessSimpleServiceTest
 *
 * Responsible for adding deal timers and checking if deal timer is good/bad, fires events
 *
 * @package Tests\Unit\Services\Deal
 */
class DealTimersProcessSimpleServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var DealTimersProcessServiceContract $service */
        $service = app(DealTimersProcessSimpleService::class);

        $this->assertInstanceOf(DealTimersProcessSimpleService::class, $service);
        $this->assertInstanceOf(DealTimersProcessServiceContract::class, $service);
    }

    /** @test */
    public function testAttachesTimersToDeal()
    {
        /** @var Deal $deal */
        $deal = factory(Deal::class)->create();
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::FINISHING, ['created_at' => now()->addSeconds(999)]);
        $deal->statuses()->attach(DealStatusConstants::FINISHED, ['created_at' => now()]);

        /** @var DealTimersProcessServiceContract $service */
        $service = app(DealTimersProcessSimpleService::class);
        $service->process($deal);

        // assert
        $this->assertEquals(999, $deal->timers->totalTime());
        $this->assertEquals(0, $deal->timers->offerTime());
        $this->assertEquals(0, $deal->timers->clientTime());
    }

    /** @test */
    public function testFastDealCorrectEvent()
    {
        Event::fake();

        /** @var Deal $deal */
        $deal = factory(Deal::class)->create();
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::FINISHING, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::FINISHED, ['created_at' => now()]);

        /** @var DealTimersProcessServiceContract $service */
        $service = app(DealTimersProcessSimpleService::class);
        $service->process($deal);

        // assert
        Event::assertNotDispatched(\App\Events\Deal\DealAfterTimeCheck::class);
        Event::assertNotDispatched(\App\Events\Deal\DealOnTimeCheck::class);
    }

    /** @test */
    public function testSlowDealCorrectSatstus()
    {
        Event::fake();

        /** @var Deal $deal */
        $deal = factory(Deal::class)->create();
        $deal->statuses()->attach(DealStatusConstants::VERIFIED, ['created_at' => now()]);
        $deal->statuses()->attach(DealStatusConstants::FINISHING, [
            'created_at' => now()->addSeconds(config('app.deal.long_wait') + 1),
        ]);
        $deal->statuses()->attach(DealStatusConstants::FINISHED, ['created_at' => now()]);

        /** @var DealTimersProcessServiceContract $service */
        $service = app(\App\Services\Deal\Timers\DealTimersProcessSimpleService::class);
        $service->process($deal);

        // assert
        $this->assertEquals(DealStatusConstants::FINISHED, $deal->status->id);
    }
}
